#!/usr/bin/env python 

import os
import sys
import pandas as pd
from Bio import SeqIO
import csv


# read in the barcode file and creates a dictionary file with transposition postion as a nested dictionary
def read_barcode_file(barcode_filename):
	reader = csv.reader(open(barcode_filename,"r"), delimiter = ',')
	d = {}
	for row in reader:
		#print (row)
		if '\xef\xbb\xbf'  in row[0]:
			#print (row)
			b1 = row[0].replace('\xef\xbb\xbf','')
			b2=row[1]
		else:
			b1, b2 = row

		d[b1]={"F1":0, "F2":0, "F3":0,"F4":0,"F5":0, "F2_pos":{},"F3_pos":{},"F4_pos":{},"F5_pos":{},"F1_UMI":{}}
	return d

# output the reverse complement of the input DNA sequences
def ReverseComplement(seq):
    seq_dict = {'A':'T','T':'A','G':'C','C':'G','N':'N'}
    return "".join([seq_dict[base] for base in reversed(seq)])

# read in the sequence reference file
def read_reference_file(ref_file):
	reader=csv.reader(open(ref_file,"r"),delimiter = ',')
	d = {}
	for row in reader:
		ref, bc= row
		d[bc]=ref.upper()
	return d

# this function itentify the transposition position and UMI information for a sequencing read and update the information in the dictionary
def add_pos_to_lib(bc_lib,r_type,key,pos,read):



	pos=int(pos)
	# get library barcode information
	BC_pos=str(read).index(key)

	UMI=""

	# get UMI information, requiring the UMI next to the known sequence "GGCC"
	if len(read[BC_pos:])>33:
		if read[BC_pos+25:BC_pos+29]=='GGCC': 


			UMI=str(read[BC_pos+29:BC_pos+33])
			

	if UMI=="":
		return bc_lib

	# this serves as the normalization factor that counts the fraction of each library element
	if r_type=="F1":
		bc_lib[key]["F1"]+=1
		if str(UMI) in bc_lib[key]["F1_UMI"].keys():
			bc_lib[key]["F1_UMI"][str(UMI)]+=1
		else:
			bc_lib[key]["F1_UMI"][str(UMI)]=1

	# for all reads that come from transpositions in forward direction and upstream of barcode and UMI,
	# add a new transpostion position and the UMI info
	# if the transpostion postion exists, get the UMI info
	# if the same transposition position and UMI info both exist, update UMI count info
	if r_type == "F2":
		bc_lib[key]["F2"]+=1
		if pos in bc_lib[key]['F2_pos'].keys():	
			
			if str(UMI) in bc_lib[key]['F2_pos'][pos].keys():
				bc_lib[key]['F2_pos'][pos][str(UMI)]+=1
			else:
				bc_lib[key]['F2_pos'][pos][str(UMI)]=1
					
		else:
			bc_lib[key]['F2_pos'][pos]={}
			bc_lib[key]['F2_pos'][pos][str(UMI)]=1

	# for all reads that come from transpositions in reverse direction and upstream of barcode and UMI,
	# add a new transpostion position and the UMI info
	# if the transpostion postion exists, get the UMI info
	# if the same transposition position and UMI info both exist, update UMI count info

	if r_type == "F3":
		bc_lib[key]["F3"]+=1

		if pos in bc_lib[key]['F3_pos'].keys():					
			if str(UMI) in bc_lib[key]['F3_pos'][pos].keys():
				bc_lib[key]['F3_pos'][pos][str(UMI)]+=1
			else:
				bc_lib[key]['F3_pos'][pos][str(UMI)]=1
					
		else:
			bc_lib[key]['F3_pos'][pos]={}
			bc_lib[key]['F3_pos'][pos][str(UMI)]=1

	# for all reads that come from transpositions in forward direction and downstream of barcode and UMI,
	# add a new transpostion position and the UMI info
	# if the transpostion postion exists, get the UMI info
	# if the same transposition position and UMI info both exist, update UMI count info
	if r_type == "F4":
		bc_lib[key]["F4"]+=1

		if pos in bc_lib[key]['F4_pos'].keys():					
			if str(UMI) in bc_lib[key]['F4_pos'][pos].keys():
				bc_lib[key]['F4_pos'][pos][str(UMI)]+=1
			else:
				bc_lib[key]['F4_pos'][pos][str(UMI)]=1
					
		else:
			bc_lib[key]['F4_pos'][pos]={}
			bc_lib[key]['F4_pos'][pos][str(UMI)]=1

	# for all reads that come from transpositions in reverse direction and downstream of barcode and UMI,
	# add a new transpostion position and the UMI info
	# if the transpostion postion exists, get the UMI info
	# if the same transposition position and UMI info both exist, update UMI count info
	if r_type == "F5":
		bc_lib[key]["F5"]+=1

		if pos in bc_lib[key]['F5_pos'].keys():					
			if str(UMI) in bc_lib[key]['F5_pos'][pos].keys():
				bc_lib[key]['F5_pos'][pos][str(UMI)]+=1
			else:
				bc_lib[key]['F5_pos'][pos][str(UMI)]=1
					
		else:
			bc_lib[key]['F5_pos'][pos]={}
			bc_lib[key]['F5_pos'][pos][str(UMI)]=1


	# return the updated barcode library
	return bc_lib



## read in R1 and R2 fastq files and determine the category and identity each read is
def parse_paired_reads_file(barcode_filename,ref_filename,read1_file,read2_file):


	# read in barcode file
	barcode_dict=read_barcode_file(barcode_filename)
	# read in read1 fastq file
	handle1 = open(read1_file,"rU")
	# read in read2 fastq file
	handle2 = open(read2_file,"rU")

	read2_iter = SeqIO.parse(handle2,"fastq")

	# read in reference file
	ref_file = read_reference_file(ref_filename)

	count = 0
	for record1 in SeqIO.parse(handle1,"fastq"): 
		record2 = next(read2_iter)



		full = (str(record1.seq)+ReverseComplement(record2.seq)).upper()
	
		
		# update the process
		count+=1
		if count%100000==1:
			print (count)			
	
		
		Reverse_seq="AAGGGCGAGGAGCTGTTCACCGG"
		F1="CTATAGGGCGAATTGGGTAC"
		F2="GACATATAAGCTAGATCGTAATTCACTACGTCAACA"
		F3="CCCATATCATGCTTTTGGGTTATCACATTCAACA"
		F4="TCTAGCTTATATGTC"
		F5="AAAGCATGATATGGG"
		
		for key in barcode_dict.keys():
	
			if key in full:	
		
				

		
				# determine if read1 starts at the constant region upstream of library insert
				if F1.upper() in full[0:30]:

					# determine if read2 starts with 5' LTR of Ty5 cDNA
					# if so this read indicates Ty5 inserts in a reverse direction at downstream of barcode and UMI 
					if F4.upper() in full[-30:]:	
						seq_to_map=full[-47:-40]
						ref_seq=ref_file[key[0:12]]
						if seq_to_map in ref_seq:
							f4_pos=str(ref_seq).index(seq_to_map)+10
							barcode_dict=add_pos_to_lib(barcode_dict,"F4",key,f4_pos,full)
			
						break

					# determine if read2 starts with 3' LTR of Ty5 cDNA
					# if so this read indicates Ty5 inserts in a forward direction at downstream of barcode and UMI 
					elif F5.upper() in full[-30:]:
						seq_to_map=full[-45:-37]
						ref_seq=ref_file[key[0:12]]
						if seq_to_map in ref_seq:
							f5_pos=str(ref_seq).index(seq_to_map)+10
							barcode_dict=add_pos_to_lib(barcode_dict,"F5",key,f5_pos,full)
							
						break

					# determine if read2 starts with the constant region
					# if so this read is for normalization purpose
					elif Reverse_seq in full[-30:]:
						barcode_dict=add_pos_to_lib(barcode_dict,"F1",key,0,full)	

						break


				# determine if read1 starts with 3' LTR of Ty5 cDNA
				# if so this read indicates Ty5 inserts in a forward direction at upstream of barcode and UMI 
				elif F2.upper() in full[0:40]:

					seq_to_map=full[37:45]
					ref_seq=ref_file[key[0:12]]
					if seq_to_map in ref_seq:
						f2_pos=str(ref_seq).index(seq_to_map)
						barcode_dict=add_pos_to_lib(barcode_dict,"F2",key,f2_pos,full)
					

					break


				# determine if read1 starts with 5' LTR of Ty5 cDNA
				# if so this read indicates Ty5 inserts in a reverse direction at upstream of barcode and UMI 
				elif F3.upper() in full[0:40]:
		
					seq_to_map=full[35:43]

					ref_seq=ref_file[key[0:12]]
					if seq_to_map in ref_seq:
						f3_pos=str(ref_seq).index(seq_to_map)
						barcode_dict=add_pos_to_lib(barcode_dict,"F3",key,f3_pos,full)

					break	
					
			          
				
	return barcode_dict


R1_file = sys.argv[1]
R2_file = sys.argv[2]
Path_To_barcodes_reference=sys.argv[3]
Path_To_sequences_reference=sys.argv[4]


datadict = parse_paired_reads_file(Path_To_barcodes_reference,Path_To_sequences_reference,R1_file,R2_file)




ref = open (Path_To_sequences_reference,"r")
table=[]

for row in csv.reader(ref):
	table.append(row)
ref.close()

new=[]
for row in table:
	barcode=row[1]
	if barcode=="barcode":
		continue



	F_1=0
	for key, value in datadict.get(barcode)["F1_UMI"].items():
		F_1=len(datadict.get(barcode)["F1_UMI"])
	F_2=0
	for key, value in datadict.get(barcode)["F2_pos"].items():
		F_2+=len(datadict.get(barcode)["F2_pos"].get(key))
	F_3=0
	for key, value in datadict.get(barcode)["F3_pos"].items():
		F_3+=len(datadict.get(barcode)["F3_pos"].get(key))
	F_4=0
	for key, value in datadict.get(barcode)["F4_pos"].items():
		F_4+=len(datadict.get(barcode)["F4_pos"].get(key))
	F_5=0
	for key, value in datadict.get(barcode)["F5_pos"].items():
		F_5+=len(datadict.get(barcode)["F5_pos"].get(key))




	new.append([row[0],barcode,datadict.get(barcode)['F1'],datadict.get(barcode)['F1_UMI'],F_1,datadict.get(barcode)['F2_pos'],F_2,datadict.get(barcode)['F3_pos'],F_3,datadict.get(barcode)['F4_pos'],F_4,datadict.get(barcode)['F5_pos'],F_5])



with open("data_example.csv","wb") as csv_file:
	writer=csv.writer(csv_file)
	for row in new:
		writer.writerow(row)


