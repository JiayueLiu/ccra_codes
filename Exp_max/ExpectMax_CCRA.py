import random
import math
import pandas as pd
from scipy.stats import gaussian_kde
import numpy as np

def rand():
    return random.random()


def estimate_gmm(x, tol=0.00000001, max_iter=1000):
    ''' Estimate GMM parameters.
        :param x: list of observed real-valued variables
        :param K: integer for number of Gaussian
        :param tol: tolerated change for log-likelihood
        :return: mu, sigma, pi parameters
    '''
    # 0. Initialize theta = (mu, sigma, pi)
    if len(x)==0:
        x=[-200]
    N = len(x)
    mu, sigma = 80, 50
    pi = 0.5

   

    dist=max(x)-min(x)
    #print (dist)
    curr_L = 99999
    for j in range(max_iter):
        
        prev_s = sigma
        # 1. E-step: responsibility = p(z_i = k | x_i, theta^(t-1))
        r = {}
        for i in range(N):
            dist=max(dist,1)
            parts = [pi * G(x[i], mu, sigma), (1-pi) * 1 /dist ]
            total = sum(parts)
            r[(i, 1)] = parts[0] / total
            r[(i, 2)] = parts[1] / total

        # 2. M-step: Update mu, sigma, pi values
        r1 = sum([r[(i, 1)] for i in range(N)])
        # print r
        pi = r1 / N
        r1 = max(r1,1e-8)
        mu = sum(r[(i, 1)] * x[i] for i in range(N)) / r1
        sigma = math.sqrt(sum([r[(i, 1)] * (x[i] - mu) ** 2 for i in range(N)]) / r1)
        


    return mu, sigma, pi, r


def G(x, mu, sigma):
    #print (sigma)
    sigma = max(sigma, 1e-8)
    return 1/(sigma * math.sqrt(2 * math.pi)) * math.e ** ( - (x - mu)**2 / (2 * sigma**2))



file = pd.read_csv("../cbf1_affinity_em.csv",header=None)
out = pd.DataFrame(columns=('seq','count','mean','sigma','p'))





for i in range(len(file)):
    F2=file.iloc[i,7]
    F3=file.iloc[i,10]
    

    all_to_plot=[F2,F3]

    pos_frame=pd.DataFrame(columns=('pos','count'))

    for item in all_to_plot:
        b=item
  
        c = b.split("},")
        for pos in c:
 
            if pos == "{}":
                continue
            cor=pos.split(":")[0].replace("{","")
            cor = int(cor)-180
            umi=pos.split(",")
            umi_num=1
            all_cor=[]
            for item in umi:

                all_cor.append((cor,umi_num))

                umi_num+=1

            
            pos_frame=pos_frame.append(pd.DataFrame(all_cor,columns=('pos','count')),ignore_index=True)

    data = pos_frame['pos'].tolist()

    result=estimate_gmm(data)

    out=out.append(pd.DataFrame([(file.iloc[i,0],len(data),result[0],result[1],result[2])],columns=('seq','count','mean','sigma','p')),ignore_index=True)


    
out.to_csv("ouput_result.csv")

